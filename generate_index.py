#! /usr/bin/env python
# -*- coding: utf-8 -*-

import json
import re
import sys

prog = re.compile(r'^\s*(#+.*)')
header_prefix = 'h-'
next_header_id = 0
toc = []
in_inline_code = False

flatten = lambda l: [item for sublist in l for item in sublist]

def convert_title(notebook_title):
    return notebook_title[4:].replace('_', ' ')

def parse_header(header, notebook_name):
    header = header.strip()
    i = 0
    while header[i] == '#':
        i += 1
    return (i,
            notebook_name,
            '{}{}'.format(header_prefix, next_header_id),
            header[i:].strip() + '\n')

def process_row(row, notebook_name):
    global next_header_id, toc, in_inline_code

    if row[:8] == '<div id=':
        row = row[row.index('\n')+1:]

    if row[:3] == "```":
        in_inline_code = not in_inline_code

    if not in_inline_code:
        result = prog.match(row)
        if result:
            toc_entry = parse_header(row, notebook_name)
            toc.append(toc_entry)
            row = row.strip() + '\n'
            row = '<div id="{}{}"></div>\n\n'.format(header_prefix,
                                                     next_header_id) + row
            next_header_id += 1
    return row

def process_cell(cell, notebook_name):
    if cell['cell_type'] == 'markdown':
        cell['source'] = [process_row(row, notebook_name)
                          for row in cell['source']]
    return cell

title = 'Superhero data science'
volume = u'Vol 1: probabilità e statistica'.encode('utf-8')

def process_notebook(notebook_name, notebook_dir):
    global next_header_id, in_inline_code

    print 'processing ' + notebook_name

    first_cell = {
      "cell_type": "markdown",
      "metadata": {'header': True},
      "source": [
        "<div class=\"header\">\n",
        "D. Malchiodi, {}. {}: {}.\n".format(title,
                                             volume,
                                             convert_title(notebook_name)),
        "</div>\n",
        "<hr style=\"width: 90%;\" align=\"left\" />"
      ]
    }

    last_cell = {
      "cell_type": "markdown",
      "metadata": {'footer': True},
      "source": [
        "<hr style=\"width: 90%;\" align=\"left\" />\n",
        "<span style=\"font-size: 0.8rem;\">D. Malchiodi, {}. {}: {}, 2017.</span>\n".format(title, volume, convert_title(notebook_name)),
        "<br>\n",
        "<div style=\"float: left;\">\n",
        "<img src=\"http://mirrors.creativecommons.org/presskit/icons/cc.large.png\" style=\"width: 1.5em; float: left; margin-right: 0.6ex; margin-top: 0;\">\n",
        "<img src=\"http://mirrors.creativecommons.org/presskit/icons/by.large.png\" style=\"width: 1.5em; float: left; margin-right: 0.6ex; margin-top: 0;\">\n",
        "<img src=\"http://mirrors.creativecommons.org/presskit/icons/nc.large.png\" style=\"width: 1.5em; float: left; margin-right: 0.6ex; margin-top: 0;\">\n",
        "<img src=\"http://mirrors.creativecommons.org/presskit/icons/nd.large.png\" style=\"width: 1.5em; float: left; margin-right: 0.6ex; margin-top: 0;\">\n",
        "<span style=\"font-size: 0.7rem; line-height: 0.7rem; vertical-align: middle;\">Quest'opera è distribuita con Licenza <a rel=\"license\" href=\"http://creativecommons.org/licenses/by-nc-nd/4.0/\">Creative Commons Attribuzione - Non commerciale - Non opere derivate 4.0 Internazionale</a></span>.\n",
        "</div>"
      ]
    }

    next_header_id = 0
    in_inline_code = False

    with open('{}/{}.ipynb'.format(notebook_dir,
                                   notebook_name)) as json_file:
        notebook = json.load(json_file)
    old_cells = [cell for cell in notebook['cells'] \
                 if 'header' not in cell['metadata'] \
                 and 'footer' not in cell['metadata']]
    new_cells = [process_cell(cell, notebook_name)
                 for cell in old_cells]
    notebook['cells'] = [first_cell] + new_cells + [last_cell]
    with open('{}/{}.ipynb'.format(notebook_dir, notebook_name),
              'w') as outfile:
        json.dump(notebook, outfile)

def process_notebooks(notebooks, notebook_dir):
    global toc

    toc = []

    for notebook_name in notebooks:
        process_notebook(notebook_name.encode('utf-8'), notebook_dir)

def to_html_entry(row):
    html_toc_line = ('<h{header}>'
                     '<a href="{file}.html#{anchor}">{name}</a>'
                     '</h{header}>')
    return html_toc_line.format(header=row[0],
                                file=row[1],
                                anchor=row[2],
                                name=row[3].encode('utf-8'))

def to_html_toc(title, volume, author):

    head_html = """
<!DOCTYPE HTML>
<html>

<head>
    <meta charset="utf-8">

    <title>Table of contents</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="custom.css">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <style>
      body { font-family: 'Alegreya', serif; }
      h2 {margin-left: 0.5cm !important;}
      h3 {margin-left: 1cm !important;}
      h4 {margin-left: 1.5cm !important;}
      h5 {margin-left: 2cm !important;}
      h6 {margin-left: 2.5cm !important;}
    </style>

</head>"""

    start_body="""
<body>
  <div class="container">
    <div class="row">
      <div class="page-header">
        <h1 style="font-size: 3rem; font-weight: 900;">{}</h1>
        <h1 style="font-size: 2.3rem; font-weight: 600;">{}</h1>
        <h1 style="font-size: 2rem; font-weight: 600;">{}</h1>
      </div>
""".format(title, volume, author)

    end_body = """
    </div>

    <hr style="width: 90%;" align="left" />
    <span style="font-size: 0.8rem;">{}, {}. {}, 2017.</span>
    <br>
    <div style="float: left;">
    <img src="http://mirrors.creativecommons.org/presskit/icons/cc.large.png" style="width: 1.5em; float: left; margin-right: 0.6ex; margin-top:0;">
    <img src="http://mirrors.creativecommons.org/presskit/icons/by.large.png" style="width: 1.5em; float: left; margin-right: 0.6ex; margin-top:0;">
    <img src="http://mirrors.creativecommons.org/presskit/icons/nc.large.png" style="width: 1.5em; float: left; margin-right: 0.6ex; margin-top:0;">
    <img src="http://mirrors.creativecommons.org/presskit/icons/nd.large.png" style="width: 1.5em; float: left; margin-right: 0.6ex; margin-top:0;">
    <span style="font-size: 0.7rem; line-height: 0.7rem; vertical-align: middle;">Quest'opera è distribuita con Licenza <a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/4.0/">Creative Commons Attribuzione - Non commerciale - Non opere derivate 4.0 Internazionale</a></span>.
    </div>
  </div>

</body>
</html>
""".format(author, title, volume)

    mid_body = '\n'.join([to_html_entry(row) for row in toc])
    return head_html + start_body + mid_body + end_body

def to_ipynb_entry(row):
    entry = {
        "cell_type": "markdown",
        "metadata": {},
        "source": []
    }

    prefix = '#'*row[0]

    url = '{name}.ipynb#{anchor}'.format(prefix=prefix,
                                                 name=row[1],
                                                 anchor=row[2])

    entry['source'].append('{prefix} [{text}]({url})'.format(prefix=prefix,
                                                             text=row[3].encode('utf-8'),
                                                             url=url))
    return entry

def to_ipynb_toc(title, author):

    toc_notebook = {
     "metadata": {
      "kernelspec": {
       "display_name": "Python 3",
       "language": "python",
       "name": "python3"
      },
      "language_info": {
       "codemirror_mode": {
        "name": "ipython",
        "version": 3
       },
       "file_extension": ".py",
       "mimetype": "text/x-python",
       "name": "python",
       "nbconvert_exporter": "python",
       "pygments_lexer": "ipython3",
       "version": "3.6.2"
      }
     },
     "nbformat": 4,
     "nbformat_minor": 2
    }

    toc_header = [{
        "cell_type": "markdown",
        "metadata": {},
        "source": ['# {}\n'.format(title),
                   '### {}\n'.format(author)]
    }]

    toc_notebook['cells'] = toc_header + [to_ipynb_entry(t) for t in toc]

    return toc_notebook

if len(sys.argv) != 2:
    print('usage: {} metadata-file'.format(sys.argv[0]))
else:
    with open(sys.argv[1]) as json_file:
        metadata = json.load(json_file)

    notebook_dir = metadata['notebook_dir']
    process_notebooks(metadata['chapters'], notebook_dir)

    with open('index.html', 'w') as html_toc:
        html_toc.write(to_html_toc('Superhero data science',
                                   u'Vol 1: probabilità e statistica'.encode('utf-8'),
                                   'D. Malchiodi'))

    with open('{}/index.ipynb'.format(notebook_dir), 'w') as ipynb_toc:
        json.dump(to_ipynb_toc(metadata['title'], metadata['author']),
                                                  ipynb_toc)
